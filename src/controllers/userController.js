const Message = require('../models/messageModel');

module.exports = {
    async sendMessage(req,res){
        const { username, payload, timestamp} = req.body;

        const message = new Message(username, payload, timestamp);
        
        return res.json(message);
    },
}